package com.SerialCommunicator;
import java.io.*;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.util.Enumeration;
import gnu.io.*;
public class ArduinoCommunicator implements SerialPortEventListener {

    SerialPort serialPort;
    //the port that the arduino is installed on your computer
    private static final String PORT = "TTYAMC0";
    private InputStream input;
    private OutputStream output;
    private static final int TIME_OUT = 2000;
    private static final int DATA_RATE = 9600;

    public void initialize() {
        CommPortIdentifier portId = null;
        Enumeration portEnum = (Enumeration)CommPortIdentifier.getPortIdentifiers();
        
        while (portEnum.hasMoreElements()) {
            CommPortIdentifier currPortId = (CommPortIdentifier) portEnum.nextElement();
            if (currPortId.getName().equals(PORT)) {
                portId = currPortId;
                break;
            }
        }
        if (portId == null) {
            System.out.println("Could not find COM port.");
            return;
        }
        try {
            // open serial port, and use class name for the appName.
            serialPort = (SerialPort) portId.open(this.getClass().getName(),
                    TIME_OUT);

            // set port parameters
            serialPort.setSerialPortParams(DATA_RATE,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);

            // open the streams
            input = serialPort.getInputStream();
            output = serialPort.getOutputStream();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }

    /**
     * This should be called when you stop using the port.
     * This will prevent port locking on platforms like Linux.
     */
    public synchronized void close() {
        if (serialPort != null) {
            serialPort.removeEventListener();
            serialPort.close();
        }
    }

    //listens and constantly outputs the results from the arduino
    public void listenForever() throws IOException {
        initialize();
        System.out.println("Started");
        byte[] readBuffer = new byte[400];
        while (true) {
            int availableBytes = input.available();
            if (availableBytes > 0) {
                // Read the serial port
                input.read(readBuffer, 0, availableBytes);
                // Print it out
                System.out.print(new String(readBuffer, 0, availableBytes));
            }
        }
    }
    
    @Override
    public void serialEvent(SerialPortEvent arg0) {
    	// TODO Auto-generated method stub
    	System.out.println("serial event");
    	
    }
    

    //sends the message to the arduino
    public void sendMessage(String _message)
    {
    	try {
			output.write(_message.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    //reads a message from the arduino and retruns the message
    public String readMessage()
    {
    	byte inputBuffer[] = new byte[100];
     	try {
			input.read(inputBuffer);
			return inputBuffer.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     	return null;
    }
}




