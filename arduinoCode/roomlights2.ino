/*
  Reading a serial ASCII-encoded string.
 
 This sketch demonstrates the Serial parseInt() function.
 It looks for an ASCII string of comma-separated values.
 It parses them into ints, and uses those to fade an RGB LED.
 
 Circuit: Common-anode RGB LED wired like so:
 * Red cathode: digital pin 3
 * Green cathode: digital pin 5
 * blue cathode: digital pin 6
 * anode: +5V
 
 created 13 Apr 2012
 by Tom Igoe
 
 This example code is in the public domain.
 */

// pins for the LEDs:
const int wbLight = 1; //enum for whiteboard light
const int wbWhitePin = 9;
const int wbBluePin = 10;
const int wbRedPin = 11;

const int ldLight = 2;
const int ldWhitePin = 3;
const int ldBluePin = 5;
const int ldRedPin = 6;

const int numberOfPinsUsed = 6;
const int usedPins[numberOfPinsUsed] = {3,5,6,9,10,11};
int pinValue[14] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0};//array to hold the current value of any pin. the index is the pin number


void setup() {
  // initialize serial:
  Serial.begin(9600);
  // make the pins outputs:
  pinMode(wbWhitePin, OUTPUT); 
  pinMode(wbBluePin, OUTPUT); 
  pinMode(wbRedPin, OUTPUT);
  
  pinMode(ldWhitePin, OUTPUT); 
  pinMode(ldBluePin, OUTPUT); 
  pinMode(ldRedPin, OUTPUT);
}

void loop() {
  if(Serial.available() > 0){
     char command = Serial.read();
     switch (command){
      case 'S':
        setLights(Serial.parseInt(),Serial.parseInt(),Serial.parseInt(),Serial.parseInt());  
        break;
      case 'F':
    //    fadeUp(wbWhitePin,5);
       break;
     }
  }

}



int setLights(int lightSelector, int white, int blue, int red){
      switch(lightSelector){
        case wbLight:
          changePin(wbRedPin, red);
          changePin(wbBluePin, blue);
          changePin(wbWhitePin, white);
      Serial.println("SETwblight");
          break;
        case ldLight:
          changePin(ldRedPin, red);
          changePin(ldBluePin, blue);
          changePin(ldWhitePin, white);
      Serial.println("SETldlight");
          break;
      }
 
    return 0;    
}



int changePin(int pin, int value){
  int delaytime = 5;
  int mValue = 255 - constrain(value,0,255);
 
 if(pinValue[pin] < mValue){
      for(int i = pinValue[pin]; i <= mValue; i++){
        analogWrite(pin,i);
        delay(delaytime);
     }  
 }else{//fade up
      for(int i = pinValue[pin]; i >= mValue; i--){
        analogWrite(pin,i);      
        delay(delaytime);
     }     
 }
  pinValue[pin] = mValue; 
 return 0; 
}
